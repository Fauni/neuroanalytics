#!/bin/bash

outDir=$1 # sets variable for output directory


## Make a directory fakedata given the output directory
fakeDir=${outDir}/fakedata # variable for fakedata directory in the output directory
mkdir $fakeDir # make fakedata directory

for i in $(seq 1 50); do
    donorDir=${fakeDir}/donor${i} # create donor directory in fakedata Dir
    mkdir ${donorDir} # create donor directory

## Finds all the files that have the string pattern "donor[number]" in the output directory and puts them in the donor directory
    find ${outDir} -type f -name "donor${i}_*" -exec mv -t ${donorDir} {} +

done
