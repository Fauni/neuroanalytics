#!/bin/bash

ped=$1 # input file
filename=`basename $ped` # file name

numLines=`wc -l $ped | awk '{print $1}'` #get number of lines of the file

for i in $(seq 1 ${numLines}); do # loop through each line 
    echo $filename # print file name
    sleep 1 # rest for 1 sec
    gen=$(head -n${i} $ped | tail -n1 | cut -d" " -f2-10000000 | sed 's/0/AA/g' | sed 's/1/AB/g' | sed 's/2/BB/g') # change numbers into genotypes
    patientName=$(head -n${i} $ped | tail -n1 | awk '{print $1}') # prevent sed from changing the patient name
    echo -e "${patientName} ${gen}"
    sleep 2
done
