#!/bin/bash

donor=`seq 1 50` # make number sequence for donors from 1 to 50
tp=`seq 1 10` # make number sequence for timepoints from 1 to 10

## Loops through each number for each donor and then for each timepoint

for i in $donor; do 
    for j in $tp; do
        filename="donor${i}_tp${j}.txt" # create a variable for file name
        touch $filename # create file
        echo "data" > ${filename} # echo data into file

        ## Echo 5 random numbers into each file
        echo "$RANDOM" >> ${filename}
        echo "$RANDOM" >> ${filename}
        echo "$RANDOM" >> ${filename}
        echo "$RANDOM" >> ${filename}
        echo "$RANDOM" >> ${filename}
    done
done
