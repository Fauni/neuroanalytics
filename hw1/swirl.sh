#!/bin/bash

## Using the command "man echo," the inputs of echo can be variables, quotes, numbers, resuls of other commands, and lines.

echo -e "Spinning clockwise..."

echo -ne '/';
sleep 1;
echo -ne '\b-';
sleep 1;
echo -ne '\b\\'
sleep 1;
echo -ne '\b|';
sleep 1;
echo -ne '\b/';
sleep 1;
echo -ne '\b-';
sleep 1;
echo -ne '\b\\';
sleep 1;
echo -ne '\b|';
echo -ne "\b \n";

echo -e "Spinning backwards..."

echo -ne '\b\\'; # echo backspace and escape backward slash
sleep 1; # rest for 1 sec
echo -ne '\b-'; # echo backspace and -
sleep 1; # rest for 1 sec
echo -ne '\b/'; # echo backspace and forward slash
sleep 1; # rest for 1 sec
echo -ne '\b|'; # echo basckspace and |
sleep 1; # rest for 1 sec
echo -ne '\b\\' # echo backspace and escape backward slash
sleep 1; # rest for 1 sec
echo -ne '\b-'; # echo backspace and -
sleep 1; # rest for 1 sec
echo -ne '\b/'; # echo backspace and forward slash
sleep 1; # rest for 1 sec
echo -ne '\b|'; # echo backspace and |
echo -ne "\b \n";

echo -e "Spinning clockwise and quickly..."

echo -ne '/';
sleep 0.1;
echo -ne '\b-';
sleep 0.1;
echo -ne '\b\\'
sleep 0.1;
echo -ne '\b|';
sleep 0.1;
echo -ne '\b/';
sleep 0.1;
echo -ne '\b-';
sleep 0.1;
echo -ne '\b\\';
sleep 0.1;
echo -ne '\b|';
echo -ne "\b \n";

echo -e "Looping through 10 revolutions..."

for i in $(seq 1 10); do
    echo -ne '/';
    sleep ${i};
    echo -ne '\b-';
    sleep ${i};
    echo -ne '\b\\'
    sleep ${i};
    echo -ne '\b|';
    sleep ${i};
    echo -ne '\b/';
    sleep ${i};
    echo -ne '\b-';
    sleep ${i};
    echo -ne '\b\\';
    sleep ${i};
    echo -ne '\b|';
    echo -ne "\b \n";
done
