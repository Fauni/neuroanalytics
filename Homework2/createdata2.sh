#!/bin/bash

donor=`seq 1 100` # donor sequence from 1 to 100
tp=`seq 1 10` # timepoint sequence from 1 to 10

for i in $donor; do # loop through donor sequence
    for j in $tp; do # loop through timepoint sequence
            ## if statement for when the donor number is less than or equal to 9 and the timepoint number is less than or equal to 9
        if [ $i -le 9 ] && [ $j -le 9 ]; then 
            filename=donor00${i}_tp00${j}.txt # makes the appropriate filename
            echo "data" > ${filename} # echo data
            #echo 5 random numbers
            echo "$RANDOM" >> ${filename}
            echo "$RANDOM" >> ${filename}
            echo "$RANDOM" >> ${filename}
            echo "$RANDOM" >> ${filename}
            echo "$RANDOM" >> ${filename}
            ## if statement for when the donor number is less than or equal to 9 and the timepoint number is greater than 9
        elif [ $i -le 9 ] && [ $j -gt 9 ]; then
            filename=donor00${i}_tp0${j}.txt
            echo "data" > ${filename}
            echo "$RANDOM" >> ${filename}
            echo "$RANDOM" >> ${filename}
            echo "$RANDOM" >> ${filename}
            echo "$RANDOM" >> ${filename}
            echo "$RANDOM" >> ${filename}
            ## if statement for when the donor number is greater than 9 and the timepoint number is greater than 9 and the donor number is less than 100
        elif [ $i -gt 9 ] && [ $j -gt 9 ] && [ $i -lt 100 ]; then
            filename=donor0${i}_tp0${j}.txt
            echo "data" > ${filename}
            echo "$RANDOM" >> ${filename}
            echo "$RANDOM" >> ${filename}
            echo "$RANDOM" >> ${filename}
            echo "$RANDOM" >> ${filename}
            echo "$RANDOM" >> ${filename}
            ## if statement for when the donor number is greater than 9 and the timepoint number is less than or equal to 9 and the donor number is less than 100
        elif [ $i -gt 9 ] && [ $j -le 9 ] && [ $i -lt 100 ]; then
            filename=donor0${i}_tp00${j}.txt
            echo "data" > ${filename}
            echo "$RANDOM" >> ${filename}
            echo "$RANDOM" >> ${filename}
            echo "$RANDOM" >> ${filename}
            echo "$RANDOM" >> ${filename}
            echo "$RANDOM" >> ${filename}
            ## if statement for when the donor number is 100 and the timepoint is less than or equal to 9
        elif [ $i -eq 100 ] && [ $j -le 9 ]; then
            filename=donor${i}_tp00${j}.txt
            echo "data" > ${filename}
            echo "$RANDOM" >> ${filename}
            echo "$RANDOM" >> ${filename}
            echo "$RANDOM" >> ${filename}
            echo "$RANDOM" >> ${filename}
            echo "$RANDOM" >> ${filename}
            ## if statement for when the donor number is 100 and the timepoint is greater than 9
        elif [ $i -eq 100 ] && [ $j -gt 9 ]; then
            filename=donor${i}_tp0${j}.txt
            echo "data" > ${filename}
            echo "$RANDOM" >> ${filename}
            echo "$RANDOM" >> ${filename}
            echo "$RANDOM" >> ${filename}
            echo "$RANDOM" >> ${filename}
            echo "$RANDOM" >> ${filename}
        fi
    done
done

