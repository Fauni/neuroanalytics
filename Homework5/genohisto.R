## Load necessary packages
library(tidyverse)
library(data.table)
library(stringr)
library(ggplot2)

snpAF <- read_tsv("SNP_AF.txt") # I made a text file containing the AF for each SNP that passed the missing and high AF filter

## Main histogram: altered axis titles, specified main title, binwidth=0.001

pdf(file="genohisto.pdf",width=20,height=20)
ggplot(snpAF) + geom_histogram(mapping=aes(x=minorAF),binwidth = 0.001,fill="#00AFBB") +
  theme(panel.grid.major = element_line(color="gray",linetype="dotted"), 
        panel.grid.minor = element_blank(), 
        panel.background = element_blank(),
        axis.line = element_line(colour = "black")) + 
  labs(x="Minor Allele Frequency",y="SNP Count",
       title="Minor Allele Frequency Distribution of SNPs",caption="n=60373, binwidth=0.001")
dev.off()
