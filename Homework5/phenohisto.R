## Load necessary packages
library(tidyverse)
library(data.table)
library(stringr)
library(ggplot2)

qtphe <- fread("qt.phe") # phenotype into df

## Histogram Plot: changed the color into light blue, changed the transparency, changed the themes, and added labels and axis titles

pdf(file="phenohisto.pdf",width=20,height=20)
ggplot(qtphe) + 
  geom_histogram(mapping=aes(x=V3),binwidth=1,fill="#00AFBB",alpha=0.4) + 
  scale_x_continuous(breaks=seq(0,13,1)) + 
  theme(panel.grid.major = element_line(color="gray",linetype="dotted"), 
        panel.grid.minor = element_blank(), 
        panel.background = element_blank(),
        axis.line = element_line(colour = "black")) + 
  labs(x="Phenotype",y="Sample Count",
       title="Phenotype Score Distribution of Samples",caption="n=89, binwidth=1")
dev.off()
