## Libraries needed
##BiocManager::install("DESeq2")
##BiocManager::install("tximport")
##BiocManager::install("apeglm")
##BiocManager::install("pheatmap")
##BiocManager::install("viridis")
##BiocManager::install("gprofiler2")
##BiocManager::install("GenomicRanges")
##BiocManager::install("biomaRt")
##BiocManager::install("org.Mmu.eg.db")
##BiocManager::install("Organism.dplyr")
##BiocManager::install("BSgenome.Mmusculus.UCSC.mm9")
##BiocManager::install("GenomicAlignments")
##BiocManager::install("matrixStats")
##BiocManager::install("AnnotationDbi")
##BiocManager::install("Biostrings")
##BiocManager::install("GOplot")
##BiocManager::install("goseq")

## Load all libraries

library(DESeq2)
library(tidyverse)
library(stringr)
library(data.table)
library(tximport)
library(apeglm)
library(gprofiler2)
library(GenomicRanges)
library(ggplot2)
library(biomaRt)
library(pheatmap)
library(BSgenome)
library(org.Mmu.eg.db)
org.mmu <- org.Mmu.eg.db
library(Organism.dplyr)
library(matrixStats)
library(AnnotationDbi)
library(Biostrings)
library(viridis)
viridislibrary(GOplot)
library(goseq)
library(BSgenome.Mmusculus.UCSC.mm9)
library(TxDb.Mmusculus.UCSC.mm9.knownGene)
txdb <- TxDb.Mmusculus.UCSC.mm9.knownGene
mm9BS <- BSgenome.Mmusculus.UCSC.mm9
stringsAsFactors=FALSE
#options(max.print = 1000000)

allData <- fread("Final_mRNA_oRNA_DESeq_matrix.txt") # read file that has read matrix per gene/oRNA
condData <- fread("oRNA_col.txt") # read condition file
rownames(condData) <- c('Vehicle1','Vehicle2','Vehicle3',
                        'Topotecan1','Topotecan2','Topotecan3') # place samples in rownames

nameTreat = allData$Name # create a nameTreat column for all the transcripts
allData <- allData[,2:7] # only take the reads
rownames(allData) = nameTreat # name the rows after transcripts

dds <- DESeqDataSetFromMatrix(countData = allData, colData = condData, design = ~ condition) # create DESeq2-friendly data frame
vsd <- vst(dds) # run variance stabilizing transformation
dds <- DESeq(dds, betaPrior = FALSE) # run DESeq2
res <- results(dds, independentFiltering = FALSE, contrast=c("condition","Vehicle","Topotecan")) # store DESeq2 results
rownames(res) <- nameTreat # annotate DESeq2 results with transcripts as row names
res <- res[order(res$padj),] # order the results by FDR-adjusted p-values
resShrunk <- lfcShrink(dds, type="apeglm", coef=2, lfcThreshold = 1) # shrink log2FC by approximating posterior estimation for GLM coefficients (used for shrinking l2fc in transcripts with low read support)

## Process DESeq2 results for oRNAs
circleData <- res[grep("N",rownames(res),invert=TRUE),] # only grep for results that are not RefSeq transcripts
circleData$oRNA <- rownames(circleData) #store oRNA candidates in a column
rownames(circleData) <- c() # delete rownames

## Quality Checks for DESeq2 results

## Plot PCA
DESeq2::plotPCA(vsd, intgroup="condition") # conduct Principal Component Analysis (PCA) and plot it

## Plot Sample Distance Heatmap
sampleDists <- dist(t(assay(vsd))) # calculate distance of variances among samples
sampleDistMatrix <- as.matrix(sampleDists) # put the results as a matrix
pheatmap(sampleDistMatrix, clustering_distance_rows = sampleDists, 
         clustering_distance_cols = sampleDists,
         col=inferno(8)) # make heatmap for variances among samples

## Plot Dispersion Plot
DESeq2::plotDispEsts(dds) # looks at dispersion versus mean normalized count. As normalized count increases, the dispersion should also decrease (in good RNASeq data).

## Plot Volcano

par(mar=c(7,7,7,7)) # set margins
volData <- as.data.frame(res) # put DESeq2 results as a data frame
volData$log2FoldChange <- -(volData$log2FoldChange) # have to change the l2fc values because DESeq2 looks at groups alphabetically
with(volData,plot(volData$log2FoldChange, 
                  -log10(volData$padj), pch=20,cex=3.0, 
                  xlab=bquote(~Log[2]~Fold~Change), 
                  ylab=bquote(~-log[10]~P-value), col=alpha("black",0.4), xlim=c(-8,6), cex.axis=2.0,cex.sub=2.0,cex.lab=2.0)) # volcano plot
with(subset(res, padj<0.05 & abs(log2FoldChange)>1), points(-log2FoldChange, -log10(padj), pch=20, col=alpha("red",0.4),ce=1.0)) # subet significant DE (differentially expressed) genes
abline(v=0, col="black", lty=3, lwd=1.0) # set vertical line for l2fc=0
abline(v=-1, col="black", lty=4, lwd=2.0) # set vertical line for l2fc=-1
abline(v=1, col="black", lty=4, lwd=2.0) # set vertical line for l2fc=1
abline(h=-log10(max(volData$pvalue[volData$padj<0.05], na.rm=TRUE)), 
       col="black", lty=4, lwd=2.0) # set horizontal line for padj <0.05

## Annotate RefSeq IDs with gene symbols
volData$refseq = rownames(res) # create refseq column
src <- src_organism(TxDb.Mmusculus.UCSC.mm9.knownGene) # load mm9 annotation
src <- src_ucsc("Mus musculus",genome="mm9") # use biomart to extract annotation
refseqKeys = volData[grep("N",volData$refseq),7] # get refseq IDs
refseqAnnot <- select(src,keys=refseqKeys, columns=c("symbol","genename","ensembl","gene_start","gene_end","go","ontology","entrez"),keytype = "refseq") # create RefSeq annotation

## Create Gene Length Data Frame
geneLengthAnnot = refseqAnnot # set in a different name so RefSeq Annotation remains clean
geneLengthAnnot$length = geneLengthAnnot$gene_end - geneLengthAnnot$gene_start # calculate gene length
geneLengthRes <- merge(geneLengthAnnot,volData,by="refseq") # merge data frames by "refseq"
geneLengthRes = geneLengthRes[grep("N",geneLengthRes$refseq),] # grep only RefSeq transcripts
geneLengthRes = geneLengthRes[!is.na(geneLengthRes$padj) & !is.na(geneLengthRes$length),] # filter out results that do not have NA values for length and padj

# Total L2FC/Gene Length Plot for all Genes

lengthData <- geneLengthRes[!is.na(geneLengthRes$length),]

ggplot(lengthData,aes(x=log10(lengthData$length/1000),y=lengthData$log2FoldChange)) + 
  geom_point(size=4,color="black",alpha=0.5) + 
  labs(x="Gene Length (Kb)",y=bquote(~Log[2]~Fold~Change)) + 
  theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(), panel.background = element_blank(), 
        axis.line = element_line(colour = "black"), legend.key=element_blank(), axis.text.x=element_text(size=18,face="bold"),
        axis.text.y=element_text(size=18,face="bold"),axis.title=element_text(size=20)) + coord_fixed(ratio=0.20) + 
  geom_hline(yintercept = 0) + scale_x_continuous(breaks=c(-1,0,1,2,3),labels = c("0.1","1","10","100","1000"))

## Binning results
# Mean L2FC/Mean Gene Length Plot
meanLengthDataTmp = lengthData[order(lengthData$length),] # order genes by length

n <- 100 # genes per bin
meanLengthData <- aggregate(meanLengthDataTmp,list(rep(1:(nrow(meanLengthDataTmp)%/%n+1),each=n,len=nrow(meanLengthDataTmp))),mean)[-1]; # calculate mean length and l2fc per bin

meanFinalLengthData <- data.frame("length"=meanLengthData$length,"log2FoldChange"=meanLengthData$log2FoldChange) # fish out 2 important columns

ggplot(meanFinalLengthData,aes(x=log10(meanFinalLengthData$length/1000),y=meanFinalLengthData$log2FoldChange)) + 
  geom_point(size=4,color="black",alpha=0.5) + 
  labs(x="Mean Gene Length (Kb)",y=bquote(~Mean~Log[2]~Fold~Change),caption="Genes per bin = 100") + 
  theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(), panel.background = element_blank(), 
        axis.line = element_line(colour = "black"), legend.key=element_blank(),axis.text.x=element_text(size=18,face="bold"),
        axis.text.y=element_text(size=18,face="bold"),axis.title=element_text(size=20)) + coord_fixed(ratio=0.20) +
  geom_hline(yintercept=0) + scale_x_continuous(breaks=c(-1,0,1,2,3),labels = c("0.1","1","10","100","1000"))

## Plot number of circles per gene length

numberCircGene <- fread("annotated_highConf_oRNAs.bed") # read file
numberCircCount <- data.frame(numberCircGene %>% group_by(V6,V4) %>% tally()) #remove duplicate oRNAs across samples
tmpNumberCircCount <- numberCircCount[order(numberCircCount$V4),] # order by host gene length
n <- 25 # of genes per bin
meanCircCount <- aggregate(tmpNumberCircCount,list(rep(1:(nrow(tmpNumberCircCount)%/%n+1),each=n,len=nrow(tmpNumberCircCount))),mean)[-1]; # calculate mean for oRNAs and host gene length

ggplot(meanCircCount,aes(y=meanCircCount$n,x=log10(meanCircCount$V4))) + geom_point(size=4,color="#00AFBB",alpha=0.5) + scale_x_continuous(limits=c(1,2000),breaks=seq(0,2000,250)) + 
  stat_smooth(method = 'nls', formula = y ~ a*log(x), aes(colour = 'logarithmic'), se = FALSE, start = list(a=1,b=1)) + 
  theme(panel.grid.major = element_line(color="grey",linetype="dotted"), panel.grid.minor = element_blank(), panel.background = element_blank(),axis.line = element_line(colour = "black"),legend.position="none",axis.text.x=element_text(size=18,face="bold"),axis.text.y=element_text(size=18),axis.title=element_text(size=20),plot.title=element_text(hjust=0.5,size=28)) +
  labs(x="Mean Gene Length (kb)",y="Mean Circular Region Per Bin",caption="High Confidence Circular Regions, Bin=25") +
  scale_y_continuous(limits=c(1,3.5)) + scale_x_continuous(breaks=c(-1,0,1,2,3,3.41),labels = c("0.1","1","10","100","1000","2600"))


## Plot circ l2fc per gene length

circData <- volData[grep("chr",rownames(volData)),] # get all oRNAs from DESeq2 results
circData$y <- rownames(circData) # create a column for oRNAs

circData <- circData %>% separate(y,sep=";",c("position","strand","symbol","gLength")) # separate by position, strand, symbol, and gene length
rownames(circData) <- c() # empty rownames for circData
circData <- circData[order(circData$gLength),] # order by host gene length
tmpCircDataLength <- data.frame("gLength"=circData$gLength,"log2FoldChange"=circData$log2FoldChange,"padj"=circData$padj) # create tmp data frame
rownames(tmpCircDataLength) <- make.names(circData$gene,unique=TRUE) # create unique names for genes to allow multiple oRNAs in a gene as rownames
tmpCircDataLength$gLength <- as.numeric(as.character(tmpCircDataLength$gLength)) # turn gene length as numeric
tmpCircDataLength <- tmpCircDataLength[order(tmpCircDataLength$gLength),] # order by gene length

n <- 25 # genes per bin

circHostLength <- aggregate(tmpCircDataLength,list(rep(1:(nrow(tmpCircDataLength)%/%n+1),each=n,len=nrow(tmpCircDataLength))),mean)[-1]; # find mean for host gene length and l2fc per bin

ggplot(circHostLength,aes(x=log10(circHostLength$gLength),circHostLength$log2FoldChange)) + geom_point(size=4,color="#00AFBB",alpha=0.5) + 
  theme(panel.grid.major = element_line(color="gray",linetype="dotted"), panel.grid.minor = element_blank(), panel.background = element_blank(),axis.line = element_line(colour = "black"),legend.position="none",axis.text.x=element_text(size=18,face="bold"),axis.text.y=element_text(size=18),axis.title=element_text(size=18)) +
  labs(x="Mean Host Gene Length (Kb)",y=bquote(~Mean~Circular~Region~Log[2]~Fold~Change),caption="High Confidence Circular Regions, Bin=25") + scale_x_continuous(breaks=c(-1,0,1,2,3,3.41),labels = c("0.1","1","10","100","1000","2600"))


## Host l2fc vs oRNA l2fc

highConfoRNA_mRNA <- merge(circData,geneLengthRes,by="symbol") # merge mRNA and oRNA data frames by gene symbol
highConfoRNA_mRNA <- data.frame("symbol"=highConfoRNA_mRNA$symbol,"oRNAl2fc"=highConfoRNA_mRNA$log2FoldChange.x,"mRNAl2fc"=highConfoRNA_mRNA$log2FoldChange.y) # create a data frame on values needed for plotting

ggplot(highConfoRNA_mRNA,aes(x=mRNAl2fc,y=oRNAl2fc)) + geom_point(size=4,color="#00AFBB",alpha=0.5) + 
  geom_smooth(method="lm",color="#FF6666", alpha=0.5) + 
  theme(panel.grid.major = element_line(color="gray",linetype="dotted"), 
        panel.grid.minor = element_blank(), 
        panel.background = element_blank(),axis.line = element_line(colour = "black"),legend.position="none",axis.text.x=element_text(size=18,face="bold"),axis.text.y=element_text(size=18),axis.title=element_text(size=18)) +
  labs(x=bquote(~Host~Gene~Log[2]~Fold~Change),y=bquote(~Mean~oRNA~Region~Log[2]~Fold~Change),caption="High Confidence Circular Regions")

## Linear regression
# Find r-squared value for oRNA l2fc and mRNA l2fc
oRNA_lm <- lm(highConfoRNA_mRNA$oRNAl2fc ~ highConfoRNA_mRNA$mRNAl2fc) # create linear model
summary(oRNA_lm) # find r-suqared value

## GO plotter mRNA
geneLengthRes = geneLengthRes[!is.na(geneLengthRes$padj) & !is.na(geneLengthRes$length) & geneLengthRes$padj<0.05,] # filter out results that do not have NA values for length and padj < 0.05
gostres <- gost(unique(geneLengthRes$symbol),organism="mmusculus",ordered_query=TRUE,significant=TRUE,correction_method = "fdr",user_threshold = 0.05) # run Gene Ontology cluster analysis
p <- gostplot(gostres,capped=FALSE,interactive=FALSE) # plot cluster results
publish_gostplot(p,highlight_terms = c("GO:0005515","GO:0045202","GO:0043005","GO:0007399",
                                       "GO:0007275","GO:0022008","GO:0048731","GO:0065008","TF:M00716_1",
                                       "TF:M04662_1")) # highlight results
## GO plotter oRNA
sigoRNA <- circData[circData$padj<0.05 & !is.na(circData$padj),] # only use significant DE oRNAs
oRNA_gostres <- gost(unique(sigoRNA$symbol[1:12]),organism="mmusculus",ordered_query=TRUE,significant=TRUE,correction_method = "fdr",user_threshold = 0.05) # run cluster analysis for oRNAs
q <- gostplot(oRNA_gostres,capped=FALSE) # plot cluster analysis results
