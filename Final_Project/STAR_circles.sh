#!/bin/bash

## STAR-aligned oRNA Finder
## This script is designed to align raw FASTQs to the reference genome using STAR 2-pass and annotate the alignment files with Gencode annotations. This script outputs files about chimeric reads, multimapped reads, unmapped reads, novel splice juncitons. This script uses CircExplorer2, circRNA_finder, DCC, and STARChip to find circular RNA candidates.

## Created by: Harper Fauni


function usage(){
cat << EOF
------------------------
STAR-aligned oRNA Finder

SCRIPT DESCRIPTION:
    -This script is designed to align raw FASTQs to the reference genome using STAR 2-pass and annotate the alignment files with Gencode annotations. 
    -This script outputs files about chimeric reads, multimapped reads, unmapped reads, novel splice juncitons. 
    -This script uses CircExplorer2, circRNA_finder, DCC, and STARChip to find circular RNA candidates.
    -Available for the following genome builds: mm9, mm10, and hg38

INPUT ARGUMENTS:

-f <input file>         (REQUIRED) A list of raw FASTQs. 
                                   - If data is paired-end, the format per line (tab-delimited): 
                                   forward FASTQ, reverse FASTQ, sample name
                                   - If data is single-end, the format per line (tab-delimited):
                                   FASTQ, sample name
-r <input string>       (REQUIRED) Run identifier. Ex: human_blood_oRNA
-o <input string>       (REQUIRED) Output directory
-b <input string>       (REQUIRED) Genome Build, Can support: hg38 (human), mm9 (mouse), mm10 (mouse)
-m <input string>       (REQUIRED) Run Mode. Paired-end: "pe", Single-end: "se"

OUTPUT:

1) STAR-aligned BAM
2) Chimeric Reads File
4) Splice Junction File
5) oRNA Discovery Results from CircExplorer2, circRNA_finder, DCC, and STARChip

MODULES NEEDED:

1) STAR v/2.7.1a
2) CircExplorer2
3) circRNA_finder
4) DCC
5) STARChip

HOW TO RUN THE SCRIPT:

bash circexplorer2.sh -f <fastq list> -r <run identifier> -b <genome build> -o <output directory> -m <run mode>
bash circexplorer2.sh -f fastq_list.txt -r human_blood -b hg38 -o /nas/longleaf/circles -m pe

------------------------
EOF
}


## Set up arguments
while getopts "f:r:o:b:m:" OPTION; do
    case $OPTION in
        f) fList=$OPTARG;;
        r) runID=$OPTARG;;
        o) outDir=$OPTARG;;
        b) genBuild=$OPTARG;;
        m) runMode=$OPTARG;;
        \?) echo -e "Incorrect option used. Exiting...\n"
                usage
                exit 1;;
    esac
done

## Check input arguments
if [ $# -ne 10 ]; then
    echo -e "Incorrect number of arguments given. Exiting...\n"
    usage
    exit 1
fi

## Reference Files for STAR and oRNA tools

## mm10
mm10_repeatMasker=/proj/zylkalab/Harper/references/mm10/ucsc/UCSC_mm10_RepeatMasker_w_SimRepeats.gtf
mm10_gencodeAllGTF=/proj/zylkalab/Harper/references/mm10/gencode/gencode_All/gencode.vM22.chr_patch_hapl_scaff.annotation.gtf
mm10_STARChipBED=/proj/zylkalab/Harper/references/mm10/gencode/gencode_All/gencode.vM22.chr_patch_hapl_scaff.annotation.gtf.bed
mm10_gencodeAllSAIndex=/proj/zylkalab/Harper/references/mm10/gencode/gencode_All/SAindex_gencodeAll
mm10_gencodeAllFasta=/proj/zylkalab/Harper/references/mm10/gencode/gencode_All/GRCm38.p6.genome.fa
SAmm10_sjdbChrStartEnd=/proj/zylkalab/Harper/references/mm10/gencode/gencode_All/SAindex_gencodeAll/sjdbList.out.tab
mm10_kg=/proj/zylkalab/Harper/references/mm10/gencode/gencode_All/mm10_kg.txt

## mm9
mm9_repeatMasker=/proj/zylkalab/Harper/references/mm9/ucsc/UCSC_mm9_RepeatMasker_w_SimRepeats.gtf
mm9_gencodeAllGTF=/proj/zylkalab/Harper/references/mm9/gencode/gencode.vM1.annotation.gtf
mm9_STARChipBED=/proj/zylkalab/Harper/references/mm9/gencode/gencode.vM1.annotation.gtf.bed
mm9_gencodeAllFasta=/proj/zylkalab/Harper/references/mm9/gencode/NCBIM37.genome.fa
mm9_gencodeAllSAIndex=/proj/zylkalab/Harper/references/mm9/gencode/STARmm9_index
SAmm9_sjdbChrStartEnd=/proj/zylkalab/Harper/references/mm9/gencode/STARmm9_index/sjdbList.out.tab
mm9_kg=/proj/zylkalab/Harper/references/mm9/gencode/mm9_knownGenes.txt

## hg38
hg38_repeatMasker=/proj/zylkalab/Harper/references/hg38/ucsc/UCSC_hg38_RepeatMasker_w_SimRepeats.gtf
hg38_gencodeAllSAIndex=/proj/zylkalab/Harper/references/hg38/gencode/gencode_all/SAindex_gencodeAll
hg38_STARChipBED=/proj/zylkalab/Harper/references/hg38/gencode/gencode_all/gencode.v31.chr_patch_hapl_scaff.annotation.gtf.bed
hg38_gencodeAllGTF=/proj/zylkalab/Harper/references/hg38/gencode/gencode_all/gencode.v31.chr_patch_hapl_scaff.annotation.gtf
hg38_gencodeAllFasta=/proj/zylkalab/Harper/references/hg38/gencode/gencode_all/GRCh38.p12.genome.fa
SAhg38_sjdbChrStartEnd=/proj/zylkalab/Harper/references/hg38/gencode/gencode_all/SAindex_gencodeAll/sjdbList.out.tab
hg38_kg=/proj/zylkalab/Harper/references/hg38/gencode/gencode_all/hg38_kg.txt

if [[ "$runMode" == "pe" ]]; then

################## Setting all the necessary files and directories ########################

    while read line; do
        fastq1=$(echo $line | awk '{print $1}')
        fastq2=$(echo $line | awk '{print $2}')
        baseFileName=$(echo $line | awk '{print $3}')
        fastq1_name=$(basename $fastq1 | sed 's/.fastq//g')
        fastq2_name=$(basename $fastq2 | sed 's/.fastq//g')

        ## Necessary file names and directories

        # Directories:
        mainDir=${outDir}/${runID}
        circDir=${mainDir}/${baseFileName}_oRNA_${runID}
        starDir=${circDir}/STAR_alignment_${runID}
        fastqDir=${circDir}/Processed_Fastqs_${runID}
        exploDir=${circDir}/CircExplorer2_${runID}_results
        finderDir=${circDir}/circRNAFinder_${runID}_results
        dccDir=${circDir}/DCC_${runID}_results
        dccMate1Dir=${dccDir}/DCC_${baseFileName}_${runID}_mate1
        dccMate2Dir=${dccDir}/DCC_${baseFileName}_${runID}_mate2
        chipDir=${circDir}/STARChip_${runID}_results
        tcshDir=${circDir}/oRNA_${runID}_tcsh
        slurmDir=${circDir}/oRNA_${runID}_slurmOuts

        mkdir -p ${mainDir} ${circDir} ${starDir} ${fastqDir} ${exploDir} ${finderDir} ${dccDir} ${dccMate1Dir} ${dccMate2Dir} ${chipDir} ${tcshDir} ${slurmDir}

        # Job Files:
        prealignJob=${tcshDir}/${baseFileName}.cutadapters_qcCheck_${runID}.tcsh
        alignJob=${tcshDir}/${baseFileName}.STAR_alignment_${runID}.tcsh
        exploJob=${tcshDir}/${baseFileName}.CircExplorer2_${runID}_oRNAJob.tcsh
        finderJob=${tcshDir}/${baseFileName}.CircRNAFinder_${runID}_oRNAJob.tcsh
        dccJob=${tcshDir}/${baseFileName}.DCC_${runID}_oRNAJob.tcsh
        dccMate1AlignJob=${tcshDir}/${baseFileName}.DCC_mate1Align_${runID}.tcsh
        dccMate2AlignJob=${tcshDir}/${baseFileName}.DCC_mate2Align_${runID}.tcsh
        chipJob=${tcshDir}/${baseFileName}.STARChip_${runID}_oRNA.tcsh

        # Necessary files for the Pre-Alignment Part
        cutadaptOut_1=${fastqDir}/${fastq1_name}.AdapterTrimmed.fastq
        cutadaptOut_2=${fastqDir}/${fastq2_name}.AdapterTrimmed.fastq
        min20Fastq_1=${fastqDir}/${fastq1_name}.AdapterTrimmed.MINq20.fastq
        min20Fastq_2=${fastqDir}/${fastq2_name}.AdapterTrimmed.MINq20.fastq
        sync_min20Fastq_1=${fastqDir}/sync_${fastq1_name}.AdapterTrimmed.MINq20.fastq
        sync_min20Fastq_2=${fastqDir}/sync_${fastq2_name}.AdapterTrimmed.MINq20.fastq

        # STARChip Parameter and STAR Result Files
        chipParam=${chipDir}/${baseFileName}.STARChip_parameters.${runID}.txt
        chipDirFile=${chipDir}/${baseFileName}.STARChip_Dirs.${runID}.txt

        # DCC Sample and Mate Files
        dccSampleFile=${dccDir}/${baseFileName}.dccSampleSheet.txt
        dccMate1File=${dccDir}/${baseFileName}.dccMate1.txt
        dccMate2File=${dccDir}/${baseFileName}.dccMate2.txt
        
        # CIRCexplorer2 Files
        backspliced_bed=${exploDir}/${baseFileName}.${runID}_back_spliced_junction.bed
        backspliced_bedFileName=${baseFileName}.${runID}_back_spliced_junction.bed
        knownCircles=${baseFileName}.${runID}.${genBuild}knownCircles.txt

###########################################################################

#################### Creating all the Job Files ###########################

        # Create Pre-alignment Job File
        echo -e '#!/bin/bash\nmodule load fastx_toolkit\nmodule load cutadapt' >> ${prealignJob}
        echo -e "cutadapt -a GATCGGAAGAGC -A GATCGGAAGAGC --minimum-length 36 -o ${cutadaptOut_1} -p ${cutadaptOut_2} ${fastq1} ${fastq2}\nfastq_quality_filter -q 20 -i ${cutadaptOut_1} -o ${min20Fastq_1}\nfastq_quality_filter -q 20 -i ${cutadaptOut_2} -o ${min20Fastq_2}\nperl /proj/jmsimon/scripts/SynchronizeFilteredPairedFASTQs_v2_harper.pl ${min20Fastq_1} ${min20Fastq_2}" >> ${prealignJob} 

        # Create Alignment Job File
        echo -e '#!/bin/bash\nmodule load star/2.7.1a' >> ${alignJob}
        echo -e '#!/bin/bash\nmodule load star/2.7.1a' >> ${dccMate1AlignJob}
        echo -e '#!/bin/bash\nmodule load star/2.7.1a' >> ${dccMate2AlignJob}
        if [[ "$genBuild" == "hg38" ]]; then 
            ## Genome Alignment
            echo -e "STAR --runThreadN 15 --sjdbGTFfile ${hg38_gencodeAllGTF} --sjdbFileChrStartEnd ${SAhg38_sjdbChrStartEnd} --genomeDir ${hg38_gencodeAllSAIndex} --sjdbOverhang 99 --readFilesIn ${sync_min20Fastq_1} ${sync_min20Fastq_2} --outSAMtype BAM SortedByCoordinate --chimSegmentMin 15 --chimOutType Junctions SeparateSAMold --outFileNamePrefix ${starDir}/${baseFileName}. --outWigType wiggle --twopassMode Basic --outReadsUnmapped Fastx --outSJfilterOverhangMin 15 15 15 15 --alignSJoverhangMin 15 --alignSJDBoverhangMin 15 --outFilterScoreMin 1 --outFilterMatchNmin 1 --outFilterMismatchNmax 2 --chimScoreMin 15 --chimScoreSeparation 10 --chimJunctionOverhangMin 15" >> ${alignJob}

            ## hg38 DCC Mate1 Alignment
            echo -e "STAR --runThreadN 15 --sjdbGTFfile ${hg38_gencodeAllGTF} --sjdbFileChrStartEnd ${SAhg38_sjdbChrStartEnd} --genomeDir ${hg38_gencodeAllSAIndex} --sjdbOverhang 99 --readFilesIn ${sync_min20Fastq_1} --outSAMtype BAM SortedByCoordinate --chimSegmentMin 15 --chimOutType Junctions SeparateSAMold --outFileNamePrefix ${dccMate1Dir}/${baseFileName}.dccMate1. --outWigType wiggle --twopassMode Basic --outReadsUnmapped Fastx --outSJfilterOverhangMin 15 15 15 15 --alignSJoverhangMin 15 --alignSJDBoverhangMin 15 --outFilterScoreMin 1 --outFilterMatchNmin 1 --outFilterMismatchNmax 2 --chimScoreMin 15 --chimScoreSeparation 10 --chimJunctionOverhangMin 15" >> ${dccMate1AlignJob}

            ## hg38 DCC Mate2 Alignment
            echo -e "STAR --runThreadN 15 --sjdbGTFfile ${hg38_gencodeAllGTF} --sjdbFileChrStartEnd ${SAhg38_sjdbChrStartEnd} --genomeDir ${hg38_gencodeAllSAIndex} --sjdbOverhang 99 --readFilesIn ${sync_min20Fastq_2} --outSAMtype BAM SortedByCoordinate --chimSegmentMin 15 --chimOutType Junctions SeparateSAMold --outFileNamePrefix ${dccMate2Dir}/${baseFileName}.dccMate2. --outWigType wiggle --twopassMode Basic --outReadsUnmapped Fastx --outSJfilterOverhangMin 15 15 15 15 --alignSJoverhangMin 15 --alignSJDBoverhangMin 15 --outFilterScoreMin 1 --outFilterMatchNmin 1 --outFilterMismatchNmax 2 --chimScoreMin 15 --chimScoreSeparation 10 --chimJunctionOverhangMin 15" >> ${dccMate2AlignJob}

            ## Create hg38 DCC Job File
            echo -e "${starDir}/${baseFileName}.Chimeric.out.junction" >> ${dccSampleFile}
            echo -e "${dccMate1Dir}/${baseFileName}.dccMate1.Chimeric.out.junction" >> ${dccMate1File}
            echo -e "${dccMate2Dir}/${baseFileName}.dccMate2.Chimeric.out.junction" >> ${dccMate2File}
            echo -e '#!/bin/bash' >> ${dccJob}
            echo -e "DCC @${dccSampleFile} -mt1 @${dccMate1File} -mt2 @${dccMate2File} -D -ss -T 8 -O ${dccDir} -t ${dccDir} -an ${hg38_gencodeAllGTF} -R ${hg38_repeatMasker} -Pi -F -M -Nr 1 1 -fg" >> ${dccJob}

            ## Create hg38 STARChip Job File
            echo -e "${starDir}" >> ${chipDirFile}
            echo -e "readsCutoff = 1\nminSubjectLimit = 1\ndo_splice = True\ncpus = 10\ncpmCutoff = 0\nsubjectCPMcutoff = 0\nannotate = true\nstarprefix = ${baseFileName}.\nrefbed = ${hg38_STARChipBED}\nrefFasta = ${hg38_gencodeAllFasta}\nrunSTAR = false\nIDstepsback = 1" >> ${chipParam}
            echo -e '#!/bin/bash\nmodule add bedtools\nmodule add r/3.5.0' >> ${chipJob}
            echo -e "cd ${chipDir}\nperl /proj/zylkalab/Harper/AS_biomarker/programs/starchip/starchip-circles.pl ${chipDirFile} ${chipParam}\nbash Step1.sh\nbash Step2.sh" >> ${chipJob}
        
            ## Create hg38 CIRCexplorer2 Job File
            echo -e '#!/bin/bash\nmodule load python/2.7.12' >> ${exploJob}
            echo -e "cd ${starDir}\nCIRCexplorer2 parse -t STAR ${baseFileName}.Chimeric.out.junction -b ${backspliced_bed}\ncd ${exploDir}\nCIRCexplorer2 annotate -b ${backspliced_bedFileName} -r ${hg38_kg} -g ${hg38_gencodeAllFasta} --low-confidence --no-fix -o ${knownCircles}" >> ${exploJob}
       
        elif [[ "$genBuild" == "mm9" ]]; then
            # mm9 Genome Alignment
            echo -e "STAR --runThreadN 15 --genomeDir ${mm9_gencodeAllSAIndex} --sjdbGTFfile ${mm9_gencodeAllGTF} --sjdbFileChrStartEnd ${SAmm9_sjdbChrStartEnd} --sjdbOverhang 99 --readFilesIn ${sync_min20Fastq_1} ${sync_min20Fastq_2} --outSAMtype BAM SortedByCoordinate --chimSegmentMin 15 --chimOutType Junctions SeparateSAMold --outFileNamePrefix ${starDir}/${baseFileName}. --outWigType wiggle --twopassMode Basic --outReadsUnmapped Fastx --outSJfilterOverhangMin 15 15 15 15 --alignSJoverhangMin 15 --alignSJDBoverhangMin 15 --outFilterScoreMin 1 --outFilterMatchNmin 1 --outFilterMismatchNmax 2 --chimScoreMin 15 --chimScoreSeparation 10 --chimJunctionOverhangMin 15" >> ${alignJob}

            # mm9 DCC Mate1 Alignment
            echo -e "STAR --runThreadN 15 --genomeDir ${mm9_gencodeAllSAIndex} --sjdbGTFfile ${mm9_gencodeAllGTF} --sjdbFileChrStartEnd ${SAmm9_sjdbChrStartEnd} --sjdbOverhang 99 --readFilesIn ${sync_min20Fastq_1} --outSAMtype BAM SortedByCoordinate --chimSegmentMin 15 --chimOutType Junctions SeparateSAMold --outFileNamePrefix ${dccMate1Dir}/${baseFileName}.dccMate1. --outWigType wiggle --twopassMode Basic --outReadsUnmapped Fastx --outSJfilterOverhangMin 15 15 15 15 --alignSJoverhangMin 15 --alignSJDBoverhangMin 15 --outFilterScoreMin 1 --outFilterMatchNmin 1 --outFilterMismatchNmax 2 --chimScoreMin 15 --chimScoreSeparation 10 --chimJunctionOverhangMin 15" >> ${dccMate1AlignJob}

            # mm9 DCC Mate2 Alignment
            echo -e "STAR --runThreadN 15 --genomeDir ${mm9_gencodeAllSAIndex} --sjdbGTFfile ${mm9_gencodeAllGTF} --sjdbFileChrStartEnd ${SAmm9_sjdbChrStartEnd} --sjdbOverhang 99 --readFilesIn ${sync_min20Fastq_2} --outSAMtype BAM SortedByCoordinate --chimSegmentMin 15 --chimOutType Junctions SeparateSAMold --outFileNamePrefix ${dccMate2Dir}/${baseFileName}.dccMate2. --outWigType wiggle --twopassMode Basic --outReadsUnmapped Fastx --outSJfilterOverhangMin 15 15 15 15 --alignSJoverhangMin 15 --alignSJDBoverhangMin 15 --outFilterScoreMin 1 --outFilterMatchNmin 1 --outFilterMismatchNmax 2 --chimScoreMin 15 --chimScoreSeparation 10 --chimJunctionOverhangMin 15" >> ${dccMate2AlignJob}
        
            # Create mm9 DCC Job File
            echo -e "${starDir}/${baseFileName}.Chimeric.out.junction" >> ${dccSampleFile}
            echo -e "${dccMate1Dir}/${baseFileName}.dccMate1.Chimeric.out.junction" >> ${dccMate1File}
            echo -e "${dccMate2Dir}/${baseFileName}.dccMate2.Chimeric.out.junction" >> ${dccMate2File}
            echo -e '#!/bin/bash' >> ${dccJob}
            echo -e "DCC @${dccSampleFile} -mt1 @${dccMate1File} -mt2 @${dccMate2File} -D -ss -T 8 -O ${dccDir} -t ${dccDir} -an ${mm9_gencodeAllGTF} -R ${mm9_repeatMasker} -Pi -F -M -Nr 1 1 -fg" >> ${dccJob}

            # Create mm9 STARChip Job File
            echo -e "${starDir}" >> ${chipDirFile}
            echo -e "readsCutoff = 1\nminSubjectLimit = 1\ndo_splice = True\ncpus = 10\ncpmCutoff = 0\nsubjectCPMcutoff = 0\nannotate = true\nstarprefix = ${baseFileName}.\nrefbed = ${mm9_STARChipBED}\nrefFasta = ${mm9_gencodeAllFasta}\nrunSTAR = false\nIDstepsback = 1" >> ${chipParam}
            echo -e '#!/bin/bash\nmodule add bedtools\nmodule add r/3.5.0' >> ${chipJob}
            echo -e "cd ${chipDir}\nperl /proj/zylkalab/Harper/AS_biomarker/programs/starchip/starchip-circles.pl ${chipDirFile} ${chipParam}\nbash Step1.sh\nbash Step2.sh" >> ${chipJob}
            
            ## Create mm9 CIRCexplorer2 Job File
            echo -e '#!/bin/bash\nmodule load python/2.7.12' >> ${exploJob}
            echo -e "cd ${starDir}\nCIRCexplorer2 parse -t STAR ${baseFileName}.Chimeric.out.junction -b ${backspliced_bed}\ncd ${exploDir}\nCIRCexplorer2 annotate -b ${backspliced_bedFileName} -r ${mm9_kg} -g ${mm9_gencodeAllFasta} --low-confidence --no-fix -o ${knownCircles}" >> ${exploJob}

        elif [[ "$genBuild" == "mm10" ]]; then
            # mm10 Genome Alignment
            echo -e "STAR --runThreadN 15 --genomeDir ${mm10_gencodeAllSAIndex} --sjdbGTFfile ${mm10_gencodeAllGTF} --sjdbFileChrStartEnd ${SAmm10_sjdbChrStartEnd} --sjdbOverhang 99 --readFilesIn ${sync_min20Fastq_1} ${sync_min20Fastq_2} --outSAMtype BAM SortedByCoordinate --chimSegmentMin 15 --chimOutType Junctions SeparateSAMold --outFileNamePrefix ${starDir}/${baseFileName}. --outWigType wiggle --twopassMode Basic --outReadsUnmapped Fastx --outSJfilterOverhangMin 15 15 15 15 --alignSJoverhangMin 15 --alignSJDBoverhangMin 15 --outFilterScoreMin 1 --outFilterMatchNmin 1 --outFilterMismatchNmax 2 --chimScoreMin 15 --chimScoreSeparation 10 --chimJunctionOverhangMin 15" >> ${alignJob}

            # mm10 DCC Mate1 Alignment
            echo -e "STAR --runThreadN 15 --genomeDir ${mm10_gencodeAllSAIndex} --sjdbGTFfile ${mm10_gencodeAllGTF} --sjdbFileChrStartEnd ${SAmm10_sjdbChrStartEnd} --sjdbOverhang 99 --readFilesIn ${sync_min20Fastq_1} --outSAMtype BAM SortedByCoordinate --chimSegmentMin 15 --chimOutType Junctions SeparateSAMold --outFileNamePrefix ${dccMate1Dir}/${baseFileName}.dccMate1. --outWigType wiggle --twopassMode Basic --outReadsUnmapped Fastx --outSJfilterOverhangMin 15 15 15 15 --alignSJoverhangMin 15 --alignSJDBoverhangMin 15 --outFilterScoreMin 1 --outFilterMatchNmin 1 --outFilterMismatchNmax 2 --chimScoreMin 15 --chimScoreSeparation 10 --chimJunctionOverhangMin 15" >> ${dccMate1AlignJob}

            # mm10 DCC Mate 2 Alignment
            echo -e "STAR --runThreadN 15 --genomeDir ${mm10_gencodeAllSAIndex} --sjdbGTFfile ${mm10_gencodeAllGTF} --sjdbFileChrStartEnd ${SAmm10_sjdbChrStartEnd} --sjdbOverhang 99 --readFilesIn ${sync_min20Fastq_2} --outSAMtype BAM SortedByCoordinate --chimSegmentMin 15 --chimOutType Junctions SeparateSAMold --outFileNamePrefix ${dccMate2Dir}/${baseFileName}.dccMate2. --outWigType wiggle --twopassMode Basic --outReadsUnmapped Fastx --outSJfilterOverhangMin 15 15 15 15 --alignSJoverhangMin 15 --alignSJDBoverhangMin 15 --outFilterScoreMin 1 --outFilterMatchNmin 1 --outFilterMismatchNmax 2 --chimScoreMin 15 --chimScoreSeparation 10 --chimJunctionOverhangMin 15" >> ${dccMate2AlignJob}
        
            # Create mm10 DCC Job File
            echo -e "${starDir}/${baseFileName}.Chimeric.out.junction" >> ${dccSampleFile}
            echo -e "${dccMate1Dir}/${baseFileName}.dccMate1.Chimeric.out.junction" >> ${dccMate1File}
            echo -e "${dccMate2Dir}/${baseFileName}.dccMate2.Chimeric.out.junction" >> ${dccMate2File}
            echo -e '#!/bin/bash' >> ${dccJob}
            echo -e "DCC @${dccSampleFile} -mt1 @${dccMate1File} -mt2 @${dccMate2File} -D -ss -T 8 -O ${dccDir} -t ${dccDir} -an ${mm10_gencodeAllGTF} -R ${mm10_repeatMasker} -Pi -F -M -Nr 1 1 -fg" >> ${dccJob}
        
            # Create mm10 STARChip Job File
            echo -e "${starDir}" >> ${chipDirFile}
            echo -e "readsCutoff = 1\nminSubjectLimit = 1\ndo_splice = True\ncpus = 10\ncpmCutoff = 0\nsubjectCPMcutoff = 0\nannotate = true\nstarprefix = ${baseFileName}.\nrefbed = ${mm10_STARChipBED}\nrefFasta = ${mm10_gencodeAllFasta}\nrunSTAR = false\nIDstepsback = 1" >> ${chipParam}
            echo -e '#!/bin/bash\nmodule add bedtools\nmodule add r/3.5.0' >> ${chipJob}
            echo -e "cd ${chipDir}\nperl /proj/zylkalab/Harper/AS_biomarker/programs/starchip/starchip-circles.pl ${chipDirFile} ${chipParam}\nbash Step1.sh\nbash Step2.sh" >> ${chipJob}
            
            ## Create mm10 CIRCexplorer2 Job File
            echo -e '#!/bin/bash\nmodule load python/2.7.12' >> ${exploJob}
            echo -e "cd ${starDir}\nCIRCexplorer2 parse -t STAR ${baseFileName}.Chimeric.out.junction -b ${backspliced_bed}\ncd ${exploDir}\nCIRCexplorer2 annotate -b ${backspliced_bedFileName} -r ${mm10_kg} -g ${mm10_gencodeAllFasta} --low-confidence --no-fix -o ${knownCircles}" >> ${exploJob}

        else
            echo -e "You did not specify any valid genome build. Exiting...\n"
            usage
            exit 1
        fi

##########################################################################

#################### Sending all the Job Files ###########################
        echo -e "------------------------------\nJobs for ${baseFileName}:\n------------------------------"

        ## Send Pre-alignment Procesing Job
        prealign_sbatch=$(sbatch --mem=30g --time=5-00:00:00 --mail-type=BEGIN,END,FAIL --mail-user=hfauni@live.unc.edu --output ${slurmDir}/${baseFileName}.prealign.slurm.out ${prealignJob})
        prealign_ID=$(echo $prealign_sbatch | awk '{print $4}')
        echo -e "Adapter Cut and QC Check Job for ${baseFileName}: ${prealign_ID}"

        ## Send STAR Alignment Job
        align_sbatch=$(sbatch --mem=100g -n 15 --time=7-00:00:00 --dependency=afterok:${prealign_ID} --mail-type=BEGIN,END,FAIL --mail-user=hfauni@live.unc.edu --output ${slurmDir}/${baseFileName}.STAR_alignment.slurm.out ${alignJob})
        align_ID=$(echo $align_sbatch | awk '{print $4}')
        echo -e "STAR Alignment Job for ${baseFileName}: $align_ID" 

        ## Send DCC Mate Jobs
        dccMate1Align_sbatch=$(sbatch --mem=70g -n 15 --time=7-00:00:00 --dependency=afterok:${prealign_ID} --mail-type=BEGIN,END,FAIL --mail-user=hfauni@live.unc.edu --output ${slurmDir}/${baseFileName}.STAR_dccMate1.slurm.out ${dccMate1AlignJob})
        dccMate1Align_ID=$(echo $dccMate1Align_sbatch | awk '{print $4}')
        echo -e "DCC Mate 1 STAR Alignment Job for ${baseFileName}: $dccMate1Align_ID"
        dccMate2Align_sbatch=$(sbatch --mem=70g -n 15 --time=7-00:00:00 --dependency=afterok:${prealign_ID} --mail-type=BEGIN,END,FAIL --mail-user=hfauni@live.unc.edu --output ${slurmDir}/${baseFileName}.STAR_dccMate2.slurm.out ${dccMate2AlignJob})
        dccMate2Align_ID=$(echo $dccMate2Align_sbatch | awk '{print $4}')
        echo -e "DCC Mate 2 STAR Alignment Job for ${baseFileName}: $dccMate2Align_ID"

        ## Send job for circRNA_finder
        echo -e '#!/bin/bash' >> ${finderJob}
        echo -e "perl /proj/zylkalab/Harper/AS_biomarker/programs/circRNA_finder/postProcessStarAlignment.pl --starDir ${starDir}/ --minLen 36 --outDir ${finderDir}/" >> ${finderJob}
        finder_sbatch=$(sbatch --mem=25g --time=12:00:00 --dependency=afterok:${align_ID} --mail-type=BEGIN,END,FAIL --mail-user=hfauni@live.unc.edu --output ${slurmDir}/${baseFileName}.circRNA_finder.slurm.out ${finderJob})
        finder_ID=$(echo $finder_sbatch | awk '{print $4}')
        echo -e "circRNA_finder oRNA Discovery Job for ${baseFileName}: $finder_ID" 

        ## Send job for DCC
        dcc_sbatch=$(sbatch -n 8 --mem=25g --time=12:00:00 --dependency=afterok:${align_ID},${dccMate1Align_ID},${dccMate2Align_ID} --mail-type=BEGIN,END,FAIL --mail-user=hfauni@live.unc.edu --output ${slurmDir}/${baseFileName}.DCC.slurm.out ${dccJob})
        dcc_ID=$(echo $dcc_sbatch | awk '{print $4}')
        echo -e "DCC oRNA Discovery Job for ${baseFileName}: $dcc_ID"

        ## Send job for STARChip
        chip_sbatch=$(sbatch -n 10 --mem=25g --time=24:00:00 --dependency=afterok:${align_ID} --mail-type=BEGIN,END,FAIL --mail-user=hfauni@live.unc.edu --output ${slurmDir}/${baseFileName}.STARChip.slurm.out ${chipJob})
        chip_ID=$(echo $chip_sbatch | awk '{print $4}')
        echo -e "STARChip oRNA Discovery Job for ${baseFileName}: $chip_ID"
        
        ## Send job for CIRCexplorer2
        explo_sbatch=$(sbatch -n 1 --mem=25g --time=12:00:00 --dependency=afterok:${align_ID} --mail-type=BEGIN,END,FAIL --mail-user=hfauni@live.unc.edu --output ${slurmDir}/${baseFileName}.CIRCexplorer2.slurm.out ${exploJob})
        explo_ID=$(echo $explo_sbatch | awk '{print $4}')
        echo -e "CIRCexplorer2 oRNA Discovery Job for ${baseFileName}: $explo_ID"
        echo -e "\n------------------------------\n"
        

##########################################################################

    done < $fList
elif [[ "$runMode" == "se" ]]; then
    while read line; do
        fastq=$(echo $line | awk '{print $1}')
        baseFileName=$(echo $line | awk '{print $2}')
        fastqName=$(basename $fastq | sed 's/.fastq//g')

        ## Necessary file names and directories

        # Directories:
        mainDir=${outDir}/${runID}
        circDir=${mainDir}/${baseFileName}_oRNA_${runID}
        starDir=${circDir}/STAR_alignment_${runID}
        fastqDir=${circDir}/Processed_Fastqs_${runID}
        exploDir=${circDir}/CircExplorer2_${runID}_results
        finderDir=${circDir}/circRNAFinder_${runID}_results
        dccDir=${circDir}/DCC_${runID}_results
        chipDir=${circDir}/STARChip_${runID}_results
        tcshDir=${circDir}/oRNA_${runID}_tcsh
        slurmDir=${circDir}/oRNA_${runID}_slurmOuts

        mkdir -p ${mainDir} ${circDir} ${starDir} ${fastqDir} ${exploDir} ${finderDir} ${dccDir} ${dccMate1Dir} ${dccMate2Dir} ${chipDir} ${tcshDir} ${slurmDir}

        # Job Files:
        prealignJob=${tcshDir}/${baseFileName}.cutadapters_qcCheck_${runID}.tcsh
        alignJob=${tcshDir}/${baseFileName}.STAR_alignment_${runID}.tcsh
        exploJob=${tcshDir}/${baseFileName}.CircExplorer2_${runID}_oRNAJob.tcsh
        finderJob=${tcshDir}/${baseFileName}.CircRNAFinder_${runID}_oRNAJob.tcsh
        dccJob=${tcshDir}/${baseFileName}.DCC_${runID}_oRNAJob.tcsh
        chipJob=${tcshDir}/${baseFileName}.STARChip_${runID}_oRNA.tcsh

        # Necessary files for the Pre-Alignment Part
        cutadaptOut_1=${fastqDir}/${fastqName}.AdapterTrimmed.fastq
        min20Fastq_1=${fastqDir}/${fastqName}.AdapterTrimmed.MINq20.fastq

        # STARChip Parameter and STAR Result Files
        chipParam=${chipDir}/${baseFileName}.STARChip_parameters.${runID}.txt
        chipDirFile=${chipDir}/${baseFileName}.STARChip_Dirs.${runID}.txt

        # DCC Sample and Mate Files
        dccSampleFile=${dccDir}/${baseFileName}.dccSampleSheet.txt

        # CIRCexplorer2 Files
        backspliced_bed=${exploDir}/${baseFileName}.${runID}_back_spliced_junction.bed
        backspliced_bedFileName=${baseFileName}.${runID}_back_spliced_junction.bed
        knownCircles=${baseFileName}.${runID}.${genBuild}knownCircles.txt


###########################################################################

#################### Creating all the Job Files ###########################

        # Create Pre-alignment Job File
        echo -e '#!/bin/bash\nmodule load fastx_toolkit\nmodule load cutadapt' >> ${prealignJob}
        echo -e "cutadapt -a GATCGGAAGAGC --minimum-length 36 -o ${cutadaptOut_1} ${fastq}\nfastq_quality_filter -q 20 -i ${cutadaptOut_1} -o ${min20Fastq_1}" >> ${prealignJob} 

        # Create Alignment Job File
        echo -e '#!/bin/bash\nmodule load star/2.7.1a' >> ${alignJob}
        if [[ "$genBuild" == "hg38" ]]; then 
            ## Genome Alignment
            echo -e "STAR --runThreadN 15 --sjdbGTFfile ${hg38_gencodeAllGTF} --sjdbFileChrStartEnd ${SAhg38_sjdbChrStartEnd} --genomeDir ${hg38_gencodeAllSAIndex} --sjdbOverhang 99 --readFilesIn ${min20Fastq_1} --outSAMtype BAM SortedByCoordinate --chimSegmentMin 15 --chimOutType Junctions SeparateSAMold --outFileNamePrefix ${starDir}/${baseFileName}. --outWigType wiggle --twopassMode Basic --outReadsUnmapped Fastx --outSJfilterOverhangMin 15 15 15 15 --alignSJoverhangMin 15 --alignSJDBoverhangMin 15 --outFilterScoreMin 1 --outFilterMatchNmin 1 --outFilterMismatchNmax 2 --chimScoreMin 15 --chimScoreSeparation 10 --chimJunctionOverhangMin 15" >> ${alignJob}

            ## Create hg38 DCC Job File
            echo -e "${starDir}/${baseFileName}.Chimeric.out.junction" >> ${dccSampleFile}
            echo -e '#!/bin/bash\nmodule load samtools' >> ${dccJob}
            echo -e "samtools index ${starDir}/${baseFileName}.Aligned.sortedByCoord.out.bam\nDCC @${dccSampleFile} -D -T 8 -O ${dccDir} -t ${dccDir} -an ${hg38_gencodeAllGTF} -R ${hg38_repeatMasker} -F -M -Nr 1 1 -G -A ${hg38_gencodeAllFasta} -fg -B ${starDir}/${baseFileName}.Aligned.sortedByCoord.out.bam" >> ${dccJob}

            ## Create hg38 STARChip Job File
            echo -e "${starDir}" >> ${chipDirFile}
            echo -e "readsCutoff = 1\nminSubjectLimit = 1\ndo_splice = True\ncpus = 10\ncpmCutoff = 0\nsubjectCPMcutoff = 0\nannotate = true\nstarprefix = ${baseFileName}.\nrefbed = ${hg38_STARChipBED}\nrefFasta = ${hg38_gencodeAllFasta}\nrunSTAR = false\nIDstepsback = 1" >> ${chipParam}
            echo -e '#!/bin/bash\nmodule add bedtools\nmodule add r/3.5.0' >> ${chipJob}
            echo -e "cd ${chipDir}\nperl /proj/zylkalab/Harper/AS_biomarker/programs/starchip/starchip-circles.pl ${chipDirFile} ${chipParam}\nbash Step1.sh\nbash Step2.sh" >> ${chipJob}
      
            ## Create hg38 CIRCexplorer2 Job File
            echo -e '#!/bin/bash\nmodule load python/2.7.12' >> ${exploJob}
            echo -e "cd ${starDir}\nCIRCexplorer2 parse -t STAR ${baseFileName}.Chimeric.out.junction -b ${backspliced_bed}\ncd ${exploDir}\nCIRCexplorer2 annotate -b ${backspliced_bedFileName} -r ${hg38_kg} -g ${hg38_gencodeAllFasta} --low-confidence --no-fix -o ${knownCircles}" >> ${exploJob}

        elif [[ "$genBuild" == "mm9" ]]; then
            # mm9 Genome Alignment
            echo -e "STAR --runThreadN 15 --genomeDir ${mm9_gencodeAllSAIndex} --sjdbGTFfile ${mm9_gencodeAllGTF} --sjdbFileChrStartEnd ${SAmm9_sjdbChrStartEnd} --sjdbOverhang 99 --readFilesIn ${min20Fastq_1} --outSAMtype BAM SortedByCoordinate --chimSegmentMin 15 --chimOutType Junctions SeparateSAMold --outFileNamePrefix ${starDir}/${baseFileName}. --outWigType wiggle --twopassMode Basic --outReadsUnmapped Fastx --outSJfilterOverhangMin 15 15 15 15 --alignSJoverhangMin 15 --alignSJDBoverhangMin 15 --outFilterScoreMin 1 --outFilterMatchNmin 1 --outFilterMismatchNmax 2 --chimScoreMin 15 --chimScoreSeparation 10 --chimJunctionOverhangMin 15" >> ${alignJob}

            # Create mm9 DCC Job File
            echo -e "${starDir}/${baseFileName}.Chimeric.out.junction" >> ${dccSampleFile}
            echo -e '#!/bin/bash\nmodule load samtools' >> ${dccJob}
            echo -e "samtools index ${starDir}/${baseFileName}.Aligned.sortedByCoord.out.bam\nDCC @${dccSampleFile} -D -T 8 -O ${dccDir} -t ${dccDir} -an ${mm9_gencodeAllGTF} -R ${mm9_repeatMasker} -F -M -Nr 1 1 -G -A ${mm9_gencodeAllFasta} -fg -B ${starDir}/${baseFileName}.Aligned.sortedByCoord.out.bam" >> ${dccJob}

            # Create mm9 STARChip Job File
            echo -e "${starDir}" >> ${chipDirFile}
            echo -e "readsCutoff = 1\nminSubjectLimit = 1\ndo_splice = True\ncpus = 10\ncpmCutoff = 0\nsubjectCPMcutoff = 0\nannotate = true\nstarprefix = ${baseFileName}.\nrefbed = ${mm9_STARChipBED}\nrefFasta = ${mm9_gencodeAllFasta}\nrunSTAR = false\nIDstepsback = 1" >> ${chipParam}
            echo -e '#!/bin/bash\nmodule add bedtools\nmodule add r/3.5.0' >> ${chipJob}
            echo -e "cd ${chipDir}\nperl /proj/zylkalab/Harper/AS_biomarker/programs/starchip/starchip-circles.pl ${chipDirFile} ${chipParam}\nbash Step1.sh\nbash Step2.sh" >> ${chipJob}
            
            ## Create mm9 CIRCexplorer2 Job File
            echo -e '#!/bin/bash\nmodule load python/2.7.12' >> ${exploJob}
            echo -e "cd ${starDir}\nCIRCexplorer2 parse -t STAR ${baseFileName}.Chimeric.out.junction -b ${backspliced_bed}\ncd ${exploDir}\nCIRCexplorer2 annotate -b ${backspliced_bedFileName} -r ${mm9_kg} -g ${mm9_gencodeAllFasta} --low-confidence --no-fix -o ${knownCircles}" >> ${exploJob}

        elif [[ "$genBuild" == "mm10" ]]; then
            # mm10 Genome Alignment
            echo -e "STAR --runThreadN 15 --genomeDir ${mm10_gencodeAllSAIndex} --sjdbGTFfile ${mm10_gencodeAllGTF} --sjdbFileChrStartEnd ${SAmm10_sjdbChrStartEnd} --sjdbOverhang 99 --readFilesIn ${min20Fastq_1} --outSAMtype BAM SortedByCoordinate --chimSegmentMin 15 --chimOutType Junctions SeparateSAMold --outFileNamePrefix ${starDir}/${baseFileName}. --outWigType wiggle --twopassMode Basic --outReadsUnmapped Fastx --outSJfilterOverhangMin 15 15 15 15 --alignSJoverhangMin 15 --alignSJDBoverhangMin 15 --outFilterScoreMin 1 --outFilterMatchNmin 1 --outFilterMismatchNmax 2 --chimScoreMin 15 --chimScoreSeparation 10 --chimJunctionOverhangMin 15" >> ${alignJob}
        
            # Create mm10 DCC Job File
            echo -e "${starDir}/${baseFileName}.Chimeric.out.junction" >> ${dccSampleFile}
            echo -e '#!/bin/bash\nmodule load samtools' >> ${dccJob}
            echo -e "samtools index ${starDir}/${baseFileName}.Aligned.sortedByCoord.out.bam\nDCC @${dccSampleFile} -D -T 8 -O ${dccDir} -t ${dccDir} -an ${mm10_gencodeAllGTF} -R ${mm10_repeatMasker} -F -M -Nr 1 1 -G -A ${mm10_gencodeAllFasta} -fg -B ${starDir}/${baseFileName}.Aligned.sortedByCoord.out.bam" >> ${dccJob}
        
            # Create mm10 STARChip Job File
            echo -e "${starDir}" >> ${chipDirFile}
            echo -e "readsCutoff = 1\nminSubjectLimit = 1\ndo_splice = True\ncpus = 10\ncpmCutoff = 0\nsubjectCPMcutoff = 0\nannotate = true\nstarprefix = ${baseFileName}.\nrefbed = ${mm10_STARChipBED}\nrefFasta = ${mm10_gencodeAllFasta}\nrunSTAR = false\nIDstepsback = 1" >> ${chipParam}
            echo -e '#!/bin/bash\nmodule add bedtools\nmodule add r/3.5.0' >> ${chipJob}
            echo -e "cd ${chipDir}\nperl /proj/zylkalab/Harper/AS_biomarker/programs/starchip/starchip-circles.pl ${chipDirFile} ${chipParam}\nbash Step1.sh\nbash Step2.sh" >> ${chipJob}
            
            ## Create mm10 CIRCexplorer2 Job File
            echo -e '#!/bin/bash\nmodule load python/2.7.12' >> ${exploJob}
            echo -e "cd ${starDir}\nCIRCexplorer2 parse -t STAR ${baseFileName}.Chimeric.out.junction -b ${backspliced_bed}\ncd ${exploDir}\nCIRCexplorer2 annotate -b ${backspliced_bedFileName} -r ${mm10_kg} -g ${mm10_gencodeAllFasta} --low-confidence --no-fix -o ${knownCircles}" >> ${exploJob}

        else
            echo -e "You did not specify any valid genome build. Exiting...\n"
            usage
            exit 1
        fi

##########################################################################

#################### Sending all the Job Files ###########################
        echo -e "------------------------------\nJobs for ${baseFileName}:\n------------------------------"

        ## Send Pre-alignment Procesing Job
        prealign_sbatch=$(sbatch --mem=30g --time=5-00:00:00 --mail-type=BEGIN,END,FAIL --mail-user=hfauni@live.unc.edu --output ${slurmDir}/${baseFileName}.prealign.slurm.out ${prealignJob})
        prealign_ID=$(echo $prealign_sbatch | awk '{print $4}')
        echo -e "Adapter Cut and QC Check Job for ${baseFileName}: ${prealign_ID}"

        ## Send STAR Alignment Job
        align_sbatch=$(sbatch --mem=100g -n 15 --time=7-00:00:00 --dependency=afterok:${prealign_ID} --mail-type=BEGIN,END,FAIL --mail-user=hfauni@live.unc.edu --output ${slurmDir}/${baseFileName}.STAR_alignment.slurm.out ${alignJob})
        align_ID=$(echo $align_sbatch | awk '{print $4}')
        echo -e "STAR Alignment Job for ${baseFileName}: $align_ID" 

        ## Send job for circRNA_finder
        echo -e '#!/bin/bash' >> ${finderJob}
        echo -e "perl /proj/zylkalab/Harper/AS_biomarker/programs/circRNA_finder/postProcessStarAlignment.pl --starDir ${starDir}/ --minLen 36 --outDir ${finderDir}/" >> ${finderJob}
        finder_sbatch=$(sbatch --mem=25g --time=12:00:00 --dependency=afterok:${align_ID} --mail-type=BEGIN,END,FAIL --mail-user=hfauni@live.unc.edu --output ${slurmDir}/${baseFileName}.circRNA_finder.slurm.out ${finderJob})
        finder_ID=$(echo $finder_sbatch | awk '{print $4}')
        echo -e "circRNA_finder oRNA Discovery Job for ${baseFileName}: $finder_ID" 

        ## Send job for DCC
        dcc_sbatch=$(sbatch -n 8 --mem=25g --time=12:00:00 --dependency=afterok:${align_ID} --mail-type=BEGIN,END,FAIL --mail-user=hfauni@live.unc.edu --output ${slurmDir}/${baseFileName}.DCC.slurm.out ${dccJob})
        dcc_ID=$(echo $dcc_sbatch | awk '{print $4}')
        echo -e "DCC oRNA Discovery Job for ${baseFileName}: $dcc_ID"

        ## Send job for STARChip
        chip_sbatch=$(sbatch -n 10 --mem=25g --time=24:00:00 --dependency=afterok:${align_ID} --mail-type=BEGIN,END,FAIL --mail-user=hfauni@live.unc.edu --output ${slurmDir}/${baseFileName}.STARChip.slurm.out ${chipJob})
        chip_ID=$(echo $chip_sbatch | awk '{print $4}')
        echo -e "STARChip oRNA Discovery Job for ${baseFileName}: $chip_ID"
        
        ## Send job for CIRCexplorer2
        explo_sbatch=$(sbatch -n 1 --mem=25g --time=12:00:00 --dependency=afterok:${align_ID} --mail-type=BEGIN,END,FAIL --mail-user=hfauni@live.unc.edu --output ${slurmDir}/${baseFileName}.CIRCexplorer2.slurm.out ${exploJob})
        explo_ID=$(echo $explo_sbatch | awk '{print $4}')
        echo -e "CIRCexplorer2 oRNA Discovery Job for ${baseFileName}: $explo_ID"
        echo -e "\n------------------------------\n"
    done < $fList
else 
    echo -e "\nYou did not specify a valid run mode. Please choose between (pe) paired-end or (se) single-end."
    usage
    exit 1
fi
